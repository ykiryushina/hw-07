export const asyncFn = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve(
      <img
        width="100%"
        alt="content"
        src="https://static.ngs.ru/news/2015/99/preview/6bc7a993d90523faf37e8b7b886bb2a10bb826ee_599_399_c.jpg"
      />
    );
  }, 3300);
});
